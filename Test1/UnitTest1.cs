﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kolo_Zadanie1;

namespace Test1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            PojemnikNaLancuchy p1 = new PojemnikNaLancuchy("Ala");

            string s = p1.ToString();

            Assert.AreEqual(s, "Ala");
        }
    }
}
