﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolo_Zadanie1
{
    public class PojemnikNaLancuchy: ICloneable,IEquatable<PojemnikNaLancuchy>,
        IComparable<PojemnikNaLancuchy>
    {
        private List<string> pojemnik;

        //public List<string> Pojemnik
        //{
        //    get
        //    {
        //        return pojemnik;
        //    }
        //    set
        //    {
        //        pojemnik = value;
        //    }
        //}

        public PojemnikNaLancuchy()
        {
            pojemnik = new List<string>();
        }

        public PojemnikNaLancuchy(string lancuch):this()
        {
            pojemnik.Add(lancuch);
        }

        public override string ToString()
        {
            //string wynik = "";
            //foreach(string s in pojemnik)
            //{
            //    wynik += s;
            //}
            //return wynik;
            StringBuilder sb = new StringBuilder();
            foreach(string s in pojemnik)
            {
                sb.Append(s);
            }
            return sb.ToString();

        }

        public void Dodaj(string lancuch)
        {
            pojemnik.Add(lancuch);
        } 

        public void Wstaw (int indeks, string lancuch)
        {
            if (indeks >= 0 && indeks < pojemnik.Count)
            {
                pojemnik[indeks] = lancuch;
            }
        }

        public void Usun(int indeks)
        {
            if (indeks >= 0 && indeks < pojemnik.Count)
            {
                pojemnik[indeks] = null;
            }
        }

        public void Usun(string lancuch)
        {
            for (int i = 0; i < pojemnik.Count; i++)
            {
                if (pojemnik[i] == lancuch)
                {
                    pojemnik[i] = null;
                }
            }
        }

        public void GC()
        {
            List<string> nowyPojemnik = new List<string>();
            foreach(string s in pojemnik)
            {
                if (s != null)
                {
                    nowyPojemnik.Add(s);
                }
            }
            pojemnik = nowyPojemnik;
        }

        public void Zamien(string staryString, string nowyString)
        {
            for (int i = 0; i < pojemnik.Count; i++)
            {
                if (pojemnik[i] == staryString)
                {
                    pojemnik[i] = nowyString;
                }
            }
        }

        public object Clone()
        {
            PojemnikNaLancuchy klon = new PojemnikNaLancuchy();
            klon.pojemnik = new List<string>(this.pojemnik);
            return klon; 
        }

        public PojemnikNaLancuchy ClonePojemnik()
        {
            return (PojemnikNaLancuchy)this.Clone();
        }

        public bool Equals(PojemnikNaLancuchy other)
        {
            this.GC();
            other.GC();
            if (this.pojemnik.Count != other.pojemnik.Count)
                return false;
            for (int i = 0; i < pojemnik.Count; i++)
                if (this.pojemnik[i] != other.pojemnik[i])
                    return false;
            return true;
        }

        public int CompareTo(PojemnikNaLancuchy other)
        {
            return this.ToString().CompareTo(other.ToString());
        }
    }
}
