﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolo_Zadanie1
{
    class Program
    {
        static void Main(string[] args)
        {
            PojemnikNaLancuchy p1 = new PojemnikNaLancuchy("Ala");

            PojemnikNaLancuchy p2 = (PojemnikNaLancuchy) p1.Clone();
            p2.Zamien("Ala", "Ela");
            PojemnikNaLancuchy p3 = new PojemnikNaLancuchy("Basia");
            // p1.Dodaj(" ma");
            //Console.WriteLine(p1);
            //Console.WriteLine(p2);
            List<PojemnikNaLancuchy> kolekcja = new List<PojemnikNaLancuchy>();
            kolekcja.Add(p1);
            kolekcja.Add(p2);
            kolekcja.Add(p3);
            //object p3 = new PojemnikNaLancuchy("Test");
            //Console.WriteLine(p3.ToString());
            //Console.WriteLine(p1.Equals(p2));
            kolekcja.Sort();
            foreach(var p in kolekcja)
            {
                Console.WriteLine(p);
            }
            Console.ReadKey();
        }
    }
}
